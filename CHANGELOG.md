# v0.0.4 (2022-05-08)

## Added (1 change)
+ Stablizing versions

# v0.0.3 (2022-02-14)

## Added (4 changes)
+ [Created a `Sound Player`](https://gitlab.com/ovid.odedbe/heavyli/-/issues/9)
+ [`Redesigned` the project](https://gitlab.com/ovid.odedbe/heavyli/-/issues/10)
+ [Made the project `DOD`](https://gitlab.com/ovid.odedbe/heavyli/-/issues/11)
+ [Created a `Mario Clone`](https://gitlab.com/ovid.odedbe/heavyli/-/issues/12)

# v0.0.2 (2021-09-29)

## Added (9 Changes)
* [Add the `Window` struct](https://gitlab.com/Ovidy/heavyli/-/issues/2)
* [Add the `Buffer` struct](https://gitlab.com/Ovidy/heavyli/-/issues/3)
* [Add the `Polygon` struct](https://gitlab.com/Ovidy/heavyli/-/issues/3)
* [Add the `Shader` struct](https://gitlab.com/Ovidy/heavyli/-/issues/3)
* [Add the `Renderer` struct](https://gitlab.com/Ovidy/heavyli/-/issues/3)
* [Add the `Transform` struct](https://gitlab.com/Ovidy/heavyli/-/issues/4)
* [Add the `Texture2D struct](https://gitlab.com/Ovidy/heavyli/-/issues/4)
* [Add the `Shape` struct](https://gitlab.com/Ovidy/heavyli/-/issues/5)
* [Add the `Camera` struct](https://gitlab.com/Ovidy/heavyli/-/issues/6)


# v0.0.1 (2021-09-23)

## Added (3 Changes)
* [Add the `Application` struct](https://gitlab.com/Ovidy/heavyli/-/issues/1)
* [Add the `Play` trait](https://gitlab.com/Ovidy/heavyli/-/issues/1)
* [Implemented API-crate exporting](https://gitlab.com/Ovidy/heavyli/-/issues/1)
