# HeavylI
`HeavylI` is a grahpics library (also with sound support) created using the `Rust` programming language and `OpenGL-GLFW`.

# HeavylI Engine
`HeavylI Engine` is a game engine (with graphics, ECS, and scripting support) based on the `HeavylI` graphics library.

## Getting started - HeavylI
Installing [Rust](https://www.rust-lang.org/) and [VSCode](https://code.visualstudio.com/) on your computer is recommended.

### Creating an Application
To create your own HeavylI Application, you should follow these steps:

#### 1. Clone the Repository:
If you haven't yet, first [install git](https://github.com/git-guides/install-git).
Then, clone the repository by typing this command in your command prompt:
```git clone <the-repository's-url>```
You could also download the project's [zip](https://gitlab.com/Ovidy/heavyli/-/archive/develop/heavyli-develop.zip).

#### 2. Creating a new Rust project.
First, you should have [cargo](https://www.rust-lang.org/tools/install) installed in your computer.
Then, go to the project's directory (HeavylI/heavyli), and type the following:
```cargo new <your-project-name>```

#### 3. Link HeavylI
In the [dependensies] section in the `Cargo.toml` file inside your project, write this:
```
....
....
....

# Engine Dependencies:
[dependencies.heavyli]
path = "../heavyli"

# OpenGL Dependencies:
[dependencies]
nalgebra-glm = "0.15.0"
lazy_static = "1.4.0"

[dependencies.glfw]
git = "https://github.com/bjz/glfw-rs.git"
default-features = false


```

#### 4. Make a Game!
Now you should be ready to start creating your own games! Take a look on the example code stored in `examples`.
You can run them via:
`cargo run --example example_name`

[
To Run the Mario Clone Example, write the following:
`cargo run --example mario`
]

**Congrats!** You've made your first HeavylI application.

**Note:** Check `heavyli/README.md` (the one inside the Rust's Project) if you have problems compiling the engine.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

