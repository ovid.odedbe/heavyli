# HeavylI
`HeavylI` is a grahpics library/ crate (also with sound support) created using `OpenGL-GLFW`.

# Usage:
This project currently aims towards creation of 2D games, with supported implementation of 2D Textures (check `heavyli::rendering::texture_2d`), different shape types (check `heavyli::rendering::shape::shape_vertices`) and sound playing (check `heavyli::sound::sound_player`).

# Code Example:
Before you write your code, you should have these two shader files in your main directory of the program:
`shaders/basic_fragment.glsl`:
```c
#version 330 core

out vec4 FragColor;

in vec3 ourColor;

void main()
{
	FragColor = vec4(ourColor, 1.0f);
}

```

`shaders/basic_vertex.glsl`:
```c
#version 330 core

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aColor;
layout (location = 2) in vec2 aTexCoord;

out vec3 ourColor;

uniform mat4 translation;

void main()
{
	gl_Position = translation * vec4(aPos, 1.0);
	ourColor = aColor;
}

```

Now, the code example for rendering a rectangle to the screen:
`src/main.rs`:
```rust
extern crate glfw;
extern crate heavyli;
extern crate nalgebra_glm as glm;
use heavyli::opengl_modules::init_glfw;
use heavyli::rendering::shader::{self, ShaderValue};
use heavyli::rendering::shape::{
    render,
    buffers::{self, Buffers},
    shape_vertices::ShapeVertices,
    vertices::{DrawType, Vertex},
};
use heavyli::transform::{projection_type::ProjectionType, transform, view};
use heavyli::rendering::window::{ClearColor, Window};

// Function to initialize the VBO and VAO with the new vertices.
//
// @param buffers a reference to the VAO and VBO.
// @param vertices the vertices of the new shape (including position, color and texture information).
fn init_buffers(buffers: &Buffers, vertices: &mut Vec<Vertex>) {
    unsafe {
        buffers::bind_buffers(buffers); // Every changes to VAO or VBO should start with binding.

        // texture_2d::attach(vertices);  // Attach the 2D texture coordinates to the vertices.

        buffers::bind_data(buffers.vao, vertices); // Bind the vertices into the vertex array object.

        buffers::update_vertex_attributes(DrawType::Triangles); // With that determine the shader's layouts.

        buffers::unbind();
    }
}

// Reserves camera information.
struct Camera {
    position: glm::Vec3,
    rotation: glm::Vec2,
}

// Screen width and height:
const SCR_WIDTH: u32 = 800;
const SCR_HEIGHT: u32 = 600;

fn main() {
    let mut glfw = init_glfw();  // Initialize GLFW handler.
    // Create a new window:
    let mut window = Window::new(&glfw, "Hello Rectangle", SCR_WIDTH, SCR_HEIGHT);

    // Window Configurations:
    window.make_current();
    window.set_key_polling(true);
    window.set_framebuffer_size_polling(true);
    window.load_function_pointers();

    let mut buffers = unsafe { buffers::generate_buffers() };  // Buffers used for VBO and VAO.
    let shader_id = shader::compile("shaders/basic_vertex.glsl", "shaders/basic_fragment.glsl");  // Initialize shader.
    // Vertices of Rectangle (default color white):
    let mut vertices = ShapeVertices::Rectangle(glm::vec2(1.0, 1.0), glm::vec2(0.0, 0.0)).value();

    // Set different colors:
    vertices[0].color = glm::vec3(0.5, 0.3, 1.0);
    vertices[1].color = glm::vec3(0.5, 0.3, 1.0);
    vertices[2].color = glm::vec3(1.0, 0.3, 1.0);
    vertices[3].color = glm::vec3(0.5, 0.7, 1.0);
    vertices[4].color = glm::vec3(0.5, 0.0, 1.0);
    vertices[5].color = glm::vec3(1.0, 0.7, 1.0);

    // Initialize the vertices into the vertex buffers:
    init_buffers(&buffers, &mut vertices);

    // Create a new transform to move the object (in our case the rectangle):
    let transform = transform::create_transform_by_position(glm::vec3(0.0, 0.0, 5.0));

    // Initialize Camera:
    let camera = Camera {
        position: glm::vec3(0.0, 0.0, -2.0),
        rotation: glm::vec2(0.0, 90.0),
    };

    while window.is_open() {  // Runs until the window closes.
        // Process input events:
        window.process_events();

        // Clear window to change background color and render next frame:
        unsafe {
            Window::clear(ClearColor {
                red: 0.0,
                green: 0.3,
                blue: 0.8,
                alpha: 1.0,
            });
        }

        // Camrea Perspective Configurations:
        const Z_NEAR: f32 = 0.1;
        const Z_FAR: f32 = 100.0;
        const FOV: f32 = 45.0;

        unsafe {
            // Set values in shader:
            shader::use_program(shader_id);
            shader::set_value(
                shader_id,
                "translation",
                ShaderValue::Mat4(transform::translate(
                    &transform,
                    Z_NEAR,
                    Z_FAR,
                    FOV,
                    view::get_view_matrix(&camera.position, camera.rotation.x, camera.rotation.y),
                    glm::vec2(SCR_WIDTH as f32, SCR_HEIGHT as f32),
                    0.0,
                    ProjectionType::Perspective,
                )),
            );

            // Draw all rectangle's data:
            render::draw_data(
                shader_id,
                buffers.vao,
                DrawType::Triangles,
                vertices.len() as i32,
                true,
            );
        }

        // Put at the end of the frame:
        window.swap_buffers();
        glfw.poll_events();
    }

    // Must be called to free memory:
    unsafe {
        buffers::delete_buffers(&mut buffers);
    }
}

```

# Features:
- Shape handling (VAO, VBO, Vertices, etc...)
- Transformation (Tranfsorm struct, view for 3D world)
- 2D Textures handling
- Sound playing (using rodio crate)

# Requirements:
* cmake
* make

# Windows Users - Compile using MSYS & MinGW:
Make sure that the [`MSYS`](https://www.msys2.org/) tool is installed.
Then follow these steps:
1. Update MSYS using:
```
pacman -Syuu
```

2. Install a toolchain:
a) for 64-bit:
```
pacman -S mingw-w64-x86_64-toolchain
```

b) for 32-bit:
```
pacman -S mingw-w64-i686-toolchain
```

For further information, check [this](https://stackoverflow.com/questions/30069830/how-to-install-mingw-w64-and-msys2) link.

3. Download the [GLFW pre-compiled binaries](https://www.glfw.org/download.html).

4. Put the banaries that satisfies your computer's bit architecture, and put them inside:
```
path/to/msys/mingw-your-version/lib
```
