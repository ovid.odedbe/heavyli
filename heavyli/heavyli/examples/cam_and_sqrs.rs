extern crate glfw;
extern crate heavyli;
extern crate nalgebra_glm as glm;
extern crate rand;

use rand::Rng;
use std::time::Instant;

use heavyli::{
    opengl_modules::init_glfw,
    rendering::{
        shader,
        shader::ShaderValue,
        shape::{
            buffers, buffers::Buffers, render, shape_vertices::ShapeVertices, vertices::DrawType,
            vertices::Vertex,
        },
        texture_2d::{self, TextureFilter},
        window::{ClearColor, Window},
    },
    sound::sound_player::SoundPlayer,
    transform::{projection_type::ProjectionType, transform, view},
};

const SCR_WIDTH: u32 = 800;
const SCR_HEIGHT: u32 = 600;

fn main() {
    run();
}

unsafe fn init_texture(texture_id: u32, texture_image_directory: &str, is_alpha: bool) {
    texture_2d::bind(texture_id);
    texture_2d::load_image(texture_image_directory.to_string(), is_alpha);
    texture_2d::unbind();
}

unsafe fn init_buffers(buffers: &Buffers, vertices: &mut Vec<Vertex>) {
    buffers::bind_buffers(buffers);

    texture_2d::attach(vertices);

    buffers::bind_data(buffers.vao, vertices);

    buffers::update_vertex_attributes(DrawType::Triangles);

    buffers::unbind();
}

fn run() {
    let mut glfw = init_glfw();
    let mut window = Window::new(&glfw, "Sandbox", SCR_WIDTH, SCR_HEIGHT);

    window.make_current();
    window.set_key_polling(true);
    window.set_framebuffer_size_polling(true);

    window.load_function_pointers();

    unsafe {
        render::enable_3d();
    }

    let mut rng = rand::thread_rng();

    let mut buffers = unsafe { buffers::generate_buffers() };
    let shader_id = shader::compile("shaders/basic_vertex.glsl", "shaders/basic_fragment.glsl");
    let mut texture_id = unsafe { texture_2d::generate_texture_id(TextureFilter::Blur) };
    let mut vertices = ShapeVertices::Rectangle(glm::vec2(1.0, 1.0), glm::vec2(0.0, 0.0)).value();

    unsafe {
        init_buffers(&buffers, &mut vertices);
    }

    unsafe {
        init_texture(texture_id, "res/wall.jpg", false);
    }

    let mut transforms = vec![transform::create_transform_by_position(glm::vec3(
        0.0, 0.0, 10.0,
    ))];

    for _i in 0..100 {
        let rand1 = rng.gen_range(-10.0..10.0);
        let rand2 = rng.gen_range(-10.0..10.0);

        transforms.push(transform::create_transform_by_position(glm::vec3(
            rand1, rand2, 10.0,
        )));
    }

    let mut pos = glm::vec3(0.0, 0.0, -5.0);
    let mut rot = glm::vec2(0.0, 90.0);

    let mut delta_time = 0.0;
    let mut delta_count = 0.0;

    let mut sound_player = SoundPlayer::new();

    let engine_index = sound_player.create_engine();

    sound_player.load_sound(engine_index, "res/sndMineTheme.ogg");

    sound_player.set_volume(engine_index, 0.05);

    while window.is_open() {
        let start_time = Instant::now();

        window.process_events();

        unsafe {
            Window::clear(ClearColor {
                red: 0.0,
                green: 0.3,
                blue: 0.8,
                alpha: 1.0,
            });
        }

        // Replay:
        if sound_player.no_sound(engine_index) {
            sound_player.load_sound(engine_index, "res/sndMineTheme.ogg");
        }

        player_input(delta_time, &window, &mut pos, &mut rot);

        const Z_NEAR: f32 = 0.1;
        const Z_FAR: f32 = 100.0;
        const FOV: f32 = 45.0;

        for transform in transforms.iter() {
            unsafe {
                texture_2d::bind(texture_id);

                shader::use_program(shader_id);
                shader::set_value(
                    shader_id,
                    "translation",
                    ShaderValue::Mat4(transform::translate(
                        &transform,
                        Z_NEAR,
                        Z_FAR,
                        FOV,
                        view::get_view_matrix(&pos, rot.x, rot.y),
                        glm::vec2(SCR_WIDTH as f32, SCR_HEIGHT as f32),
                        0.0,
                        ProjectionType::Perspective,
                    )),
                );

                render::draw_data(
                    shader_id,
                    buffers.vao,
                    DrawType::Triangles,
                    vertices.len() as i32,
                    true,
                );
            }
        }

        window.swap_buffers();
        glfw.poll_events();

        render::limit_fps(start_time, 60.0);

        delta_time = start_time.elapsed().as_nanos() as f32 / 1000000000.0;

        delta_count += delta_time;

        if delta_count >= 1.0 {
            let mut title = "Game | FPS: ".to_string();

            title.push_str(
                (1.0 / if 0.0 != delta_time {
                    delta_time
                } else {
                    0.0000001
                })
                .to_string()
                .as_str(),
            );

            window.set_title(&title);

            delta_count = 0.0;
        }
    }

    unsafe {
        buffers::delete_buffers(&mut buffers);
        texture_2d::delete_texture(&mut texture_id);
    }
}

fn player_input(delta_time: f32, window: &Window, pos: &mut glm::Vec3, rot: &mut glm::Vec2) {
    const MOVE_SPEED: f32 = 5.0;
    const ROTATE_SPEED: f32 = 100.0;

    let normalized_front = view::get_normalized_front(rot.x, rot.y);

    if window.key_pressed(glfw::Key::D) {
        rot.y += ROTATE_SPEED * delta_time;
    } else if window.key_pressed(glfw::Key::A) {
        rot.y -= ROTATE_SPEED * delta_time;
    }

    if window.key_pressed(glfw::Key::W) {
        *pos += normalized_front * MOVE_SPEED * delta_time;
    } else if window.key_pressed(glfw::Key::S) {
        *pos -= normalized_front * MOVE_SPEED * delta_time;
    }

    if window.key_pressed(glfw::Key::E) {
        *pos += view::get_right(normalized_front) * MOVE_SPEED * delta_time;
    } else if window.key_pressed(glfw::Key::Q) {
        *pos -= view::get_right(normalized_front) * MOVE_SPEED * delta_time;
    }

    if window.key_pressed(glfw::Key::Space) {
        *pos += view::get_up(normalized_front) * MOVE_SPEED * delta_time;
    } else if window.key_pressed(glfw::Key::LeftShift) {
        *pos -= view::get_up(normalized_front) * MOVE_SPEED * delta_time;
    }
}
