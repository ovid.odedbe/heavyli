/// This function initializes the GLFW library and returns its handler.
///
/// Note: Must be called if using `OpenGL` features of `HeavylI`.
pub fn init_glfw() -> glfw::Glfw {
    // Initialize GLFW:
    let mut glfw = glfw::init(glfw::FAIL_ON_ERRORS).unwrap();

    // Set glfw parameters:
    glfw.window_hint(glfw::WindowHint::ContextVersion(3, 3));
    glfw.window_hint(glfw::WindowHint::OpenGlProfile(
        glfw::OpenGlProfileHint::Core,
    ));
    #[cfg(target_os = "macos")]
    glfw.window_hint(glfw::WindowHint::OpenGlForwardCompat(true));

    return glfw;
}
