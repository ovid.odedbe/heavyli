use crate::rendering::shape::{vertices, vertices::Vertex};
use std::os::raw::c_void;

/// This class handles `OpenGL`'s buffers (the `VBO` and `VAO`).
#[derive(Clone, Copy)]
pub struct Buffers {
    pub vbo: u32,
    pub vao: u32,
}

/// 'delete_buffers' must be called at the end of its usage.
pub unsafe fn generate_buffers() -> Buffers {
    return Buffers {
        vbo: generate_vbo(),
        vao: generate_vao(),
    };
}

/// 'delete_vbo' must be called at the end of its usage.
pub unsafe fn generate_vbo() -> u32 {
    let mut vbo = 0u32;

    gl::GenBuffers(1, &mut vbo);

    return vbo;
}

/// 'delete_vao' must be called at the end of its usage.
pub unsafe fn generate_vao() -> u32 {
    let mut vao = 0u32;

    gl::GenVertexArrays(1, &mut vao);

    return vao;
}

pub unsafe fn delete_vao(vao: &mut u32) {
    if *vao != 0 {
        gl::DeleteVertexArrays(1, vao);
    }
}

pub unsafe fn delete_vbo(vbo: &mut u32) {
    if *vbo != 0 {
        gl::DeleteBuffers(1, vbo);
    }
}

pub unsafe fn delete_buffers(buffers: &mut Buffers) {
    delete_vao(&mut buffers.vao);
    delete_vbo(&mut buffers.vbo);
}

/// Binds the vertices data of a polygon.
pub unsafe fn bind_data<T>(vao: u32, vertices_data: &mut Vec<T>) {
    assert!(vertices_data.len() > 0, "Invalid vertices data.");

    bind_vao(vao);

    gl::BufferData(
        gl::ARRAY_BUFFER,
        (vertices_data.len() * std::mem::size_of::<T>()) as gl::types::GLsizeiptr,
        vertices::get_vertices_pointer(vertices_data),
        gl::STATIC_DRAW,
    );
}

pub unsafe fn bind_buffers(buffers: &Buffers) {
    bind_vao(buffers.vao);
    bind_vbo(buffers.vbo);
}

pub unsafe fn bind_vao(vao: u32) {
    gl::BindVertexArray(vao);
}

pub unsafe fn bind_vbo(vbo: u32) {
    gl::BindBuffer(gl::ARRAY_BUFFER, vbo);
}

/// Unbinds the buffers.
pub unsafe fn unbind() {
    unbind_vao();
    unbind_vbo();
}

pub unsafe fn unbind_vao() {
    gl::BindVertexArray(0);
}

pub unsafe fn unbind_vbo() {
    gl::BindBuffer(gl::ARRAY_BUFFER, 0);
}

/// Organizes the vertices in memory.
pub unsafe fn vertex_attributes<T>(index: u32, unit_size: u32, data_size: i32) {
    gl::VertexAttribPointer(
        index,
        data_size,
        gl::FLOAT,
        gl::FALSE,
        std::mem::size_of::<T>() as gl::types::GLsizei,
        (index * unit_size * std::mem::size_of::<gl::types::GLfloat>() as u32) as *const c_void,
    );
    gl::EnableVertexAttribArray(index);
}

/// Updates the buffers.
pub unsafe fn update_vertex_attributes(draw_type: vertices::DrawType) {
    vertex_attributes::<Vertex>(0, vertices::get_unit_size(draw_type), 3); // Position attribute.
    vertex_attributes::<Vertex>(1, vertices::get_unit_size(draw_type), 3); // Color attribute.
    vertex_attributes::<Vertex>(2, vertices::get_unit_size(draw_type), 2); // Texture Coordinates attribute.
}
