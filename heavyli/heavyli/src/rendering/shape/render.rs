use crate::rendering::{shader, shape::buffers, shape::vertices::DrawType};
use std::time;

pub unsafe fn draw_data(
    shader_id: u32,
    vao: u32,
    draw_type: DrawType,
    vertices_size: i32,
    fill_shape: bool,
) {
    // #1 Binding:
    bind_data_for_draw(shader_id, vao);

    if !fill_shape {
        set_lined_drawing();
    }

    // #2 Drawing:
    draw_arrays(draw_type, vertices_size);

    // #3 Unbinding:
    set_fill_drawing();
}

pub unsafe fn set_fill_drawing() {
    gl::PolygonMode(gl::FRONT_AND_BACK, gl::FILL);
}

pub unsafe fn set_lined_drawing() {
    gl::PolygonMode(gl::FRONT_AND_BACK, gl::LINE);
}

pub unsafe fn bind_data_for_draw(shader_id: u32, vao: u32) {
    shader::use_program(shader_id);

    buffers::bind_vao(vao);
}

pub unsafe fn draw_arrays(draw_type: DrawType, vertices_size: i32) {
    gl::DrawArrays(draw_type.value() as u32, 0, vertices_size);
}

pub unsafe fn enable_3d() {
    gl::Enable(gl::BLEND);
    gl::BlendFunc(gl::SRC_ALPHA, gl::ONE_MINUS_SRC_ALPHA);
    gl::Enable(gl::DEPTH_TEST);
}

pub unsafe fn disable_3d() {
    gl::Disable(gl::BLEND);
    gl::Disable(gl::DEPTH_TEST);
}

// This function delays the Game Loop to limit its FPS rate.
// Therefore, it should be called before calculating
// the Elapsed time of the Game Loop.
pub fn limit_fps(update_start_time: time::Instant, preferred_fps: f32) {
    const NANOS: f32 = 1000000000.0;

    let update_interval = ((1.0 / preferred_fps) * NANOS) as u128;

    while update_start_time.elapsed().as_nanos() < update_interval {
        spin_sleep::sleep(time::Duration::from_nanos(0));
    }
}
