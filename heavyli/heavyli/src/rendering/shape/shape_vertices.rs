use crate::rendering::shape::vertices::{Vertex, VertexL};

/// Contains vertices data for each shape.
pub enum ShapeVertices {
    Triangle(glm::Vec2, glm::Vec2, glm::Vec2),
    Rectangle(glm::Vec2, glm::Vec2),
}

// Vertices for Lighting Objects.
pub enum ShapeVerticesL {
    Cube,
}

impl ShapeVertices {
    pub fn value(&self) -> Vec<Vertex> {
        match *self {
            ShapeVertices::Triangle(left, right, top) => {
                vec![
                    Vertex {
                        position: glm::vec3(left.x, left.y, 0.0),
                        color: glm::vec3(1.0, 1.0, 1.0),
                        texture_coordinates: glm::vec2(0.0, 0.0),
                    },
                    Vertex {
                        position: glm::vec3(right.x, right.y, 0.0),
                        color: glm::vec3(1.0, 1.0, 1.0),
                        texture_coordinates: glm::vec2(1.0, 0.0),
                    },
                    Vertex {
                        position: glm::vec3(top.x, top.y, 0.0),
                        color: glm::vec3(1.0, 1.0, 1.0),
                        texture_coordinates: glm::vec2(0.5, 1.0),
                    },
                ]
            }
            ShapeVertices::Rectangle(top_left, bottom_right) => {
                vec![
                    Vertex {
                        position: glm::vec3(top_left.x, top_left.y, 0.0),
                        color: glm::vec3(1.0, 1.0, 1.0),
                        texture_coordinates: glm::vec2(0.0, 0.0),
                    },
                    Vertex {
                        position: glm::vec3(bottom_right.x, top_left.y, 0.0),
                        color: glm::vec3(1.0, 1.0, 1.0),
                        texture_coordinates: glm::vec2(1.0, 0.0),
                    },
                    Vertex {
                        position: glm::vec3(bottom_right.x, bottom_right.y, 0.0),
                        color: glm::vec3(1.0, 1.0, 1.0),
                        texture_coordinates: glm::vec2(1.0, 1.0),
                    },
                    Vertex {
                        position: glm::vec3(top_left.x, top_left.y, 0.0),
                        color: glm::vec3(1.0, 1.0, 1.0),
                        texture_coordinates: glm::vec2(0.0, 0.0),
                    },
                    Vertex {
                        position: glm::vec3(top_left.x, bottom_right.y, 0.0),
                        color: glm::vec3(1.0, 1.0, 1.0),
                        texture_coordinates: glm::vec2(0.0, 1.0),
                    },
                    Vertex {
                        position: glm::vec3(bottom_right.x, bottom_right.y, 0.0),
                        color: glm::vec3(1.0, 1.0, 1.0),
                        texture_coordinates: glm::vec2(1.0, 1.0),
                    },
                ]
            }
        }
    }
}

impl ShapeVerticesL {
    pub fn value(&self) -> Vec<VertexL> {
        match *self {
            ShapeVerticesL::Cube => {
                vec![
                    // Front:
                    VertexL {
                        position: glm::vec3(-0.5, -0.5, -0.5),
                        color: glm::vec3(1.0, 1.0, 1.0),
                        texture_coordinates: glm::vec2(0.0, 0.0),
                        normal: glm::vec3(0.0, 0.0, -1.0),
                    },
                    VertexL {
                        position: glm::vec3(0.5, -0.5, -0.5),
                        color: glm::vec3(1.0, 1.0, 1.0),
                        texture_coordinates: glm::vec2(1.0, 0.0),
                        normal: glm::vec3(0.0, 0.0, -1.0),
                    },
                    VertexL {
                        position: glm::vec3(0.5, 0.5, -0.5),
                        color: glm::vec3(1.0, 1.0, 1.0),
                        texture_coordinates: glm::vec2(1.0, 1.0),
                        normal: glm::vec3(0.0, 0.0, -1.0),
                    },
                    VertexL {
                        position: glm::vec3(0.5, 0.5, -0.5),
                        color: glm::vec3(1.0, 1.0, 1.0),
                        texture_coordinates: glm::vec2(1.0, 1.0),
                        normal: glm::vec3(0.0, 0.0, -1.0),
                    },
                    VertexL {
                        position: glm::vec3(-0.5, 0.5, -0.5),
                        color: glm::vec3(1.0, 1.0, 1.0),
                        texture_coordinates: glm::vec2(0.0, 1.0),
                        normal: glm::vec3(0.0, 0.0, -1.0),
                    },
                    VertexL {
                        position: glm::vec3(-0.5, -0.5, -0.5),
                        color: glm::vec3(1.0, 1.0, 1.0),
                        texture_coordinates: glm::vec2(0.0, 0.0),
                        normal: glm::vec3(0.0, 0.0, -1.0),
                    },
                    // Back:
                    VertexL {
                        position: glm::vec3(-0.5, -0.5, 0.5),
                        color: glm::vec3(1.0, 1.0, 1.0),
                        texture_coordinates: glm::vec2(0.0, 0.0),
                        normal: glm::vec3(0.0, 0.0, 1.0),
                    },
                    VertexL {
                        position: glm::vec3(0.5, -0.5, 0.5),
                        color: glm::vec3(1.0, 1.0, 1.0),
                        texture_coordinates: glm::vec2(1.0, 0.0),
                        normal: glm::vec3(0.0, 0.0, 1.0),
                    },
                    VertexL {
                        position: glm::vec3(0.5, 0.5, 0.5),
                        color: glm::vec3(1.0, 1.0, 1.0),
                        texture_coordinates: glm::vec2(1.0, 1.0),
                        normal: glm::vec3(0.0, 0.0, 1.0),
                    },
                    VertexL {
                        position: glm::vec3(0.5, 0.5, 0.5),
                        color: glm::vec3(1.0, 1.0, 1.0),
                        texture_coordinates: glm::vec2(1.0, 1.0),
                        normal: glm::vec3(0.0, 0.0, 1.0),
                    },
                    VertexL {
                        position: glm::vec3(-0.5, 0.5, 0.5),
                        color: glm::vec3(1.0, 1.0, 1.0),
                        texture_coordinates: glm::vec2(0.0, 1.0),
                        normal: glm::vec3(0.0, 0.0, 1.0),
                    },
                    VertexL {
                        position: glm::vec3(-0.5, -0.5, 0.5),
                        color: glm::vec3(1.0, 1.0, 1.0),
                        texture_coordinates: glm::vec2(0.0, 0.0),
                        normal: glm::vec3(0.0, 0.0, 1.0),
                    },
                    // Right:
                    VertexL {
                        position: glm::vec3(-0.5, 0.5, 0.5),
                        color: glm::vec3(1.0, 1.0, 1.0),
                        texture_coordinates: glm::vec2(1.0, 0.0),
                        normal: glm::vec3(-1.0, 0.0, 0.0),
                    },
                    VertexL {
                        position: glm::vec3(-0.5, 0.5, -0.5),
                        color: glm::vec3(1.0, 1.0, 1.0),
                        texture_coordinates: glm::vec2(1.0, 1.0),
                        normal: glm::vec3(-1.0, 0.0, 0.0),
                    },
                    VertexL {
                        position: glm::vec3(-0.5, -0.5, -0.5),
                        color: glm::vec3(1.0, 1.0, 1.0),
                        texture_coordinates: glm::vec2(0.0, 1.0),
                        normal: glm::vec3(-1.0, 0.0, 0.0),
                    },
                    VertexL {
                        position: glm::vec3(-0.5, -0.5, -0.5),
                        color: glm::vec3(1.0, 1.0, 1.0),
                        texture_coordinates: glm::vec2(0.0, 1.0),
                        normal: glm::vec3(-1.0, 0.0, 0.0),
                    },
                    VertexL {
                        position: glm::vec3(-0.5, -0.5, 0.5),
                        color: glm::vec3(1.0, 1.0, 1.0),
                        texture_coordinates: glm::vec2(0.0, 0.0),
                        normal: glm::vec3(-1.0, 0.0, 0.0),
                    },
                    VertexL {
                        position: glm::vec3(-0.5, 0.5, 0.5),
                        color: glm::vec3(1.0, 1.0, 1.0),
                        texture_coordinates: glm::vec2(1.0, 0.0),
                        normal: glm::vec3(-1.0, 0.0, 0.0),
                    },
                    // Left:
                    VertexL {
                        position: glm::vec3(0.5, 0.5, 0.5),
                        color: glm::vec3(1.0, 1.0, 1.0),
                        texture_coordinates: glm::vec2(1.0, 0.0),
                        normal: glm::vec3(1.0, 0.0, 0.0),
                    },
                    VertexL {
                        position: glm::vec3(0.5, 0.5, -0.5),
                        color: glm::vec3(1.0, 1.0, 1.0),
                        texture_coordinates: glm::vec2(1.0, 1.0),
                        normal: glm::vec3(1.0, 0.0, 0.0),
                    },
                    VertexL {
                        position: glm::vec3(0.5, -0.5, -0.5),
                        color: glm::vec3(1.0, 1.0, 1.0),
                        texture_coordinates: glm::vec2(0.0, 1.0),
                        normal: glm::vec3(1.0, 0.0, 0.0),
                    },
                    VertexL {
                        position: glm::vec3(0.5, -0.5, -0.5),
                        color: glm::vec3(1.0, 1.0, 1.0),
                        texture_coordinates: glm::vec2(0.0, 1.0),
                        normal: glm::vec3(1.0, 0.0, 0.0),
                    },
                    VertexL {
                        position: glm::vec3(0.5, -0.5, 0.5),
                        color: glm::vec3(1.0, 1.0, 1.0),
                        texture_coordinates: glm::vec2(0.0, 0.0),
                        normal: glm::vec3(1.0, 0.0, 0.0),
                    },
                    VertexL {
                        position: glm::vec3(0.5, 0.5, 0.5),
                        color: glm::vec3(1.0, 1.0, 1.0),
                        texture_coordinates: glm::vec2(1.0, 0.0),
                        normal: glm::vec3(1.0, 0.0, 0.0),
                    },
                    // Down:
                    VertexL {
                        position: glm::vec3(-0.5, -0.5, -0.5),
                        color: glm::vec3(1.0, 1.0, 1.0),
                        texture_coordinates: glm::vec2(0.0, 1.0),
                        normal: glm::vec3(0.0, -1.0, 0.0),
                    },
                    VertexL {
                        position: glm::vec3(0.5, -0.5, -0.5),
                        color: glm::vec3(1.0, 1.0, 1.0),
                        texture_coordinates: glm::vec2(1.0, 1.0),
                        normal: glm::vec3(0.0, -1.0, 0.0),
                    },
                    VertexL {
                        position: glm::vec3(0.5, -0.5, 0.5),
                        color: glm::vec3(1.0, 1.0, 1.0),
                        texture_coordinates: glm::vec2(1.0, 0.0),
                        normal: glm::vec3(0.0, -1.0, 0.0),
                    },
                    VertexL {
                        position: glm::vec3(0.5, -0.5, 0.5),
                        color: glm::vec3(1.0, 1.0, 1.0),
                        texture_coordinates: glm::vec2(1.0, 0.0),
                        normal: glm::vec3(0.0, -1.0, 0.0),
                    },
                    VertexL {
                        position: glm::vec3(-0.5, -0.5, 0.5),
                        color: glm::vec3(1.0, 1.0, 1.0),
                        texture_coordinates: glm::vec2(0.0, 0.0),
                        normal: glm::vec3(0.0, -1.0, 0.0),
                    },
                    VertexL {
                        position: glm::vec3(-0.5, -0.5, -0.5),
                        color: glm::vec3(1.0, 1.0, 1.0),
                        texture_coordinates: glm::vec2(0.0, 1.0),
                        normal: glm::vec3(0.0, -1.0, 0.0),
                    },
                    // Up:
                    VertexL {
                        position: glm::vec3(-0.5, 0.5, -0.5),
                        color: glm::vec3(1.0, 1.0, 1.0),
                        texture_coordinates: glm::vec2(0.0, 1.0),
                        normal: glm::vec3(0.0, 1.0, 0.0),
                    },
                    VertexL {
                        position: glm::vec3(0.5, 0.5, -0.5),
                        color: glm::vec3(1.0, 1.0, 1.0),
                        texture_coordinates: glm::vec2(1.0, 1.0),
                        normal: glm::vec3(0.0, 1.0, 0.0),
                    },
                    VertexL {
                        position: glm::vec3(0.5, 0.5, 0.5),
                        color: glm::vec3(1.0, 1.0, 1.0),
                        texture_coordinates: glm::vec2(1.0, 0.0),
                        normal: glm::vec3(0.0, 1.0, 0.0),
                    },
                    VertexL {
                        position: glm::vec3(0.5, 0.5, 0.5),
                        color: glm::vec3(1.0, 1.0, 1.0),
                        texture_coordinates: glm::vec2(1.0, 0.0),
                        normal: glm::vec3(0.0, 1.0, 0.0),
                    },
                    VertexL {
                        position: glm::vec3(-0.5, 0.5, 0.5),
                        color: glm::vec3(1.0, 1.0, 1.0),
                        texture_coordinates: glm::vec2(0.0, 0.0),
                        normal: glm::vec3(0.0, 1.0, 0.0),
                    },
                    VertexL {
                        position: glm::vec3(-0.5, 0.5, -0.5),
                        color: glm::vec3(1.0, 1.0, 1.0),
                        texture_coordinates: glm::vec2(0.0, 1.0),
                        normal: glm::vec3(0.0, 1.0, 0.0),
                    },
                ]
            }
        }
    }
}
