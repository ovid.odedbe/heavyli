use std::os::raw::c_void;

/// Contains vertices data.
#[derive(Clone, Copy)]
pub struct Vertex {
    pub position: glm::Vec3,
    pub color: glm::Vec3,
    pub texture_coordinates: glm::Vec2,
}

// Vertex For Lighting.
#[derive(Clone, Copy)]
pub struct VertexL {
    pub position: glm::Vec3,
    pub color: glm::Vec3,
    pub normal: glm::Vec3,
    pub texture_coordinates: glm::Vec2,
}

/// The primitive drawing types of `OpenGL`.
#[derive(Copy, Clone)]
pub enum DrawType {
    None,
    Lines,
    Triangles,
    Quads,
}

impl DrawType {
    pub fn value(&self) -> u32 {
        match *self {
            DrawType::None => gl::NONE,
            DrawType::Lines => gl::LINES,
            DrawType::Triangles => gl::TRIANGLES,
            DrawType::Quads => gl::QUADS,
        }
    }
}

/// The sizes of `OpenGL`'s primitive types.
pub enum DrawSize {
    LinesSize,
    TrianglesSize,
    QuadsSize,
}

impl DrawSize {
    pub fn value(&self) -> u32 {
        match *self {
            DrawSize::LinesSize => 2,
            DrawSize::TrianglesSize => 3,
            DrawSize::QuadsSize => 4,
        }
    }
}

/// Gets the pointer to vertices.
pub fn get_vertices_pointer<T>(vertices: &mut Vec<T>) -> *const c_void {
    return vertices.as_ptr() as *const f32 as *const c_void;
}

/// Gets the unit size by the inputted draw type.
pub fn get_unit_size(draw_type: DrawType) -> u32 {
    match draw_type {
        DrawType::None => 0,
        DrawType::Lines => DrawSize::LinesSize.value(),
        DrawType::Triangles => DrawSize::TrianglesSize.value(),
        DrawType::Quads => DrawSize::QuadsSize.value(),
    }
}

/// Gives the vertices minimum point (for attaching the shape to a texture).
pub fn get_positiving_point(vertices: &Vec<Vertex>) -> glm::Vec3 {
    let mut min = glm::vec3(0.0, 0.0, 0.0);
    let mut addition_point = glm::vec3(0.0, 0.0, 0.0);

    if vertices.len() > 0 {
        // Check which point's axises have the minimum value:
        for vertex in vertices.iter() {
            min.x = min_axis(vertex.position.x, min.x);
            min.y = min_axis(vertex.position.y, min.y);
            min.z = min_axis(vertex.position.z, min.z);
        }

        // Set the addition point (where the texture coordinates should start) as the minimum axises:
        addition_point.x = -min.x;
        addition_point.y = -min.y;
        addition_point.z = -min.z;
    } else {
        panic!("Error: empty vertices.");
    }

    return addition_point;
}

pub fn min_axis(a: f32, b: f32) -> f32 {
    return if a < b { a } else { b };
}
