use crate::rendering::shape::{vertices, vertices::Vertex};
use std::os::raw::c_void;

#[repr(u32)]
#[derive(Copy, Clone)]
pub enum TextureFilter {
    Pixelated = gl::NEAREST,
    Blur = gl::LINEAR,
}

/// 'delete_texture' must be called at the end of usage.
pub unsafe fn generate_texture_id(filter: TextureFilter) -> u32 {
    let mut id = 0u32;

    gl::GenTextures(1, &mut id); // Generate Texture ID.

    gl::BindTexture(gl::TEXTURE_2D, id); // All upcoming GL_TEXTURE_2D operations now have effect on this texture object

    // Set the texture wrapping parameters
    gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_S, gl::REPEAT as i32); // set texture wrapping to gl::REPEAT (default wrapping method)
    gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_T, gl::REPEAT as i32);

    // Set texture filtering parameters
    gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, filter as i32);
    gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, filter as i32);

    return id;
}

pub unsafe fn delete_texture(texture_id: &mut u32) {
    gl::DeleteTextures(1, texture_id);
}

/// Loads a texture image.
pub unsafe fn load_image(image_directory: String, has_alpha: bool) {
    // Read the image's data:
    let image = image::open(&std::path::Path::new(&image_directory))
        .unwrap()
        .flipv()
        .fliph()
        .to_rgb8();
    let data = image.as_raw();

    // Generate the Texture:
    gl::TexImage2D(
        gl::TEXTURE_2D,
        0,
        if has_alpha {
            gl::RGBA as i32
        } else {
            gl::RGB as i32
        },
        image.width() as i32,
        image.height() as i32,
        0,
        if has_alpha { gl::RGBA } else { gl::RGB },
        gl::UNSIGNED_BYTE,
        &data[0] as *const u8 as *const c_void,
    );

    gl::GenerateMipmap(gl::TEXTURE_2D);
}

/// Attaches the texture coordinates to the hole vertices.
pub fn attach(vertices: &mut Vec<Vertex>) {
    let positiving_point = vertices::get_positiving_point(vertices);

    for vertex in vertices {
        let coord = vertex.position + positiving_point; // Fit the texture coordinates by the data coordinates.

        vertex.texture_coordinates = glm::vec2(coord.x, coord.y);
    }
}

pub unsafe fn bind(texture_id: u32) {
    gl::ActiveTexture(gl::TEXTURE0);
    gl::BindTexture(gl::TEXTURE_2D, texture_id);
}

pub unsafe fn unbind() {
    gl::BindTexture(gl::TEXTURE_2D, 0);
}
