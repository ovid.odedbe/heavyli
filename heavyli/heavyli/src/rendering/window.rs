use glfw::Context;
use std::sync::mpsc::Receiver;

/// Contains coloring data for `OpenGL` use.
pub struct ClearColor {
    pub red: f32,
    pub green: f32,
    pub blue: f32,
    pub alpha: f32,
}

/// This class handles the `OpenGL`'s window.
pub struct Window {
    gl_window: glfw::Window,
    events: Receiver<(f64, glfw::WindowEvent)>,
}

impl Window {
    pub fn new(glfw: &glfw::Glfw, name: &str, width: u32, height: u32) -> Self {
        // Create the GLFW Window:
        let (gl_window, events) = glfw
            .create_window(width, height, name, glfw::WindowMode::Windowed)
            .expect("Failed to create GLFW window");

        // Initialize the window instance:
        return Window {
            gl_window: gl_window,
            events: events,
        };
    }

    pub fn is_open(&self) -> bool {
        return !self.gl_window.should_close();
    }

    pub fn set_title(&mut self, title: &String) {
        self.gl_window.set_title(title.as_str());
    }

    pub fn configure(&mut self) {
        // Make window operations:
        self.make_current();
        self.set_key_polling(true);
        self.set_framebuffer_size_polling(true);
        self.load_function_pointers();
    }

    /// Processes `OpenGL` events (such as user input).
    pub fn process_events(&mut self) {
        for (_, event) in glfw::flush_messages(&self.events) {
            match event {
                glfw::WindowEvent::FramebufferSize(width, height) => {
                    // Make sure the viewport matches the new window dimensions:
                    unsafe { gl::Viewport(0, 0, width, height) }
                }
                _ => {}
            }
        }
    }

    /// Clears the window's screen.
    pub unsafe fn clear(clear_color: ClearColor) {
        gl::ClearColor(
            clear_color.red,
            clear_color.green,
            clear_color.blue,
            clear_color.alpha,
        ); // Set clear color.
        gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT); // Clear the screen.
    }

    pub fn swap_buffers(&mut self) {
        self.gl_window.swap_buffers();
    }

    /// Makes the current window as the `OpenGL`'s context.
    pub fn make_current(&mut self) {
        self.gl_window.make_current();
    }

    pub fn set_key_polling(&mut self, value: bool) {
        self.gl_window.set_key_polling(value);
    }

    pub fn set_framebuffer_size_polling(&mut self, value: bool) {
        self.gl_window.set_framebuffer_size_polling(value);
    }

    pub fn load_function_pointers(&mut self) {
        // Load all OpenGL function pointers:
        gl::load_with(|symbol| self.gl_window.get_proc_address(symbol) as *const _);
    }

    /// Checks if the inputted key was pressed.
    pub fn key_pressed(&self, key: glfw::Key) -> bool {
        return glfw::Action::Press == self.gl_window.get_key(key);
    }

    /// Checks if the inputted key was released.
    pub fn key_released(&self, key: glfw::Key) -> bool {
        return glfw::Action::Release == self.gl_window.get_key(key);
    }

    // Gets the window's events.
    pub fn events(&self) -> &Receiver<(f64, glfw::WindowEvent)> {
        return &self.events;
    }
}
