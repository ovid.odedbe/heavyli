use rodio::{Decoder, OutputStream, OutputStreamHandle, Sink};
use std::fs::File;
use std::io::BufReader;

pub struct SoundPlayer {
    _stream: OutputStream,
    stream_handle: OutputStreamHandle,
    engines: Vec<Sink>,
}

impl SoundPlayer {
    pub fn new() -> Self {
        // Get a output stream handle to the default physical sound device:
        let (_stream, stream_handle) = OutputStream::try_default().unwrap();

        SoundPlayer {
            _stream: _stream,
            stream_handle: stream_handle,
            engines: Vec::<Sink>::new(),
        }
    }

    /// Creates a sound engine (rodio::Sink).
    ///
    /// @ Return: the index to the current engine.
    pub fn create_engine(&mut self) -> usize {
        self.engines
            .push(Sink::try_new(&self.stream_handle).unwrap());

        // Return the current's index:
        return self.engines.len() - 1;
    }

    /// This function loads (and plays if the engine hasn't been stopped) a sound from an engine.
    pub fn load_sound(&mut self, engine_index: usize, filename: &str) {
        assert!(
            engine_index < self.engines.len(),
            "Invalid sound engine index."
        );

        // Load a sound from a file, using a path relative to Cargo.toml:
        let file = BufReader::new(File::open(filename).unwrap());

        // Decode that sound file into a source:
        let source = Decoder::new(file).unwrap();

        // Add the sound to the engine's play queue:
        self.engines[engine_index].append(source);
    }

    /// Pauses the current engine.
    pub fn pause_engine(&mut self, engine_index: usize) {
        assert!(
            engine_index < self.engines.len(),
            "Invalid sound engine index."
        );

        // Pause:
        self.engines[engine_index].pause();
    }

    /// Resumes a stopped engine.
    pub fn resume_engine(&mut self, engine_index: usize) {
        assert!(
            engine_index < self.engines.len(),
            "Invalid sound engine index."
        );

        // Resume:
        self.engines[engine_index].play();
    }

    /// Sets the volume for an engine.
    pub fn set_volume(&mut self, engine_index: usize, volume: f32) {
        assert!(
            engine_index < self.engines.len(),
            "Invalid sound engine index."
        );

        // Set volume:
        self.engines[engine_index].set_volume(volume);
    }

    /// Gets the volume from an engine.
    pub fn get_volume(&self, engine_index: usize) -> f32 {
        assert!(
            engine_index < self.engines.len(),
            "Invalid sound engine index."
        );

        // Get volume:
        return self.engines[engine_index].volume();
    }

    // Checks if the engine doesn't play at list a single sound or is.
    pub fn no_sound(&self, engine_index: usize) -> bool {
        assert!(
            engine_index < self.engines.len(),
            "Invalid sound engine index."
        );

        return self.engines[engine_index].empty();
    }
}
