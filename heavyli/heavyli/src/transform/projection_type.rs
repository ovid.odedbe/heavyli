/// Projection Type of Perspective (usually for 3D) or Orthographic (usually for 2D).
#[derive(Clone)]
pub enum ProjectionType {
    Perspective,
    Orthographic,
}

impl ProjectionType {
    pub fn value(&self) -> u32 {
        match *self {
            ProjectionType::Perspective => 0,
            ProjectionType::Orthographic => 1,
        }
    }
}
