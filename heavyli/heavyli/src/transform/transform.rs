use crate::transform::projection_type::ProjectionType;

/// Contains transform data (position, scale and rotation).
#[derive(Clone, Copy)]
pub struct Transform {
    pub position: glm::Vec3,
    pub scale: glm::Vec3,
    pub rotation: glm::Vec4,
}

pub fn create_default_transform() -> Transform {
    return Transform {
        position: glm::vec3(0.0, 0.0, 0.0),
        scale: glm::vec3(1.0, 1.0, 1.0),
        rotation: glm::vec4(0.0, 0.0, 0.0, 0.0),
    };
}

pub fn create_transform_by_position(position: glm::Vec3) -> Transform {
    return Transform {
        position: position,
        scale: glm::vec3(1.0, 1.0, 1.0),
        rotation: glm::vec4(0.0, 0.0, 0.0, 0.0),
    };
}

/// Returns the full matrix of position.
pub fn translate(
    transform: &Transform,
    z_near: f32,
    z_far: f32,
    fov: f32,
    view_matrix: glm::Mat4,
    grid: glm::Vec2,
    ortho_distance: f32,
    projection_type: ProjectionType,
) -> glm::Mat4 {
    return get_projection(z_near, z_far, fov, grid, ortho_distance, projection_type)
        * view_matrix
        * get_model_matrix(transform);
}

pub fn get_model_matrix(transform: &Transform) -> glm::Mat4 {
    // Prepare properties:
    let mut model = glm::identity();

    // Set model (position, rotation and scale):
    model = glm::translate(&model, &transform.position);

    // Apply rotation for each axis:
    model = glm::rotate(
        &model,
        transform.rotation.x.to_radians(),
        &glm::vec3(1.0, 0.0, 0.0),
    );
    model = glm::rotate(
        &model,
        transform.rotation.y.to_radians(),
        &glm::vec3(0.0, 1.0, 0.0),
    );
    model = glm::rotate(
        &model,
        transform.rotation.z.to_radians(),
        &glm::vec3(0.0, 0.0, 1.0),
    );

    // Scale model:
    return glm::scale(&model, &transform.scale);
}

pub fn get_projection(
    z_near: f32,
    z_far: f32,
    fov: f32,
    grid: glm::Vec2,
    ortho_distance: f32,
    projection_type: ProjectionType,
) -> glm::Mat4 {
    match projection_type {
        ProjectionType::Orthographic => glm::ortho(
            -1.0 * ortho_distance,
            1.0 * ortho_distance,
            -1.0 * ortho_distance,
            1.0 * ortho_distance,
            z_near,
            z_far,
        ),
        ProjectionType::Perspective => glm::perspective(grid.x / grid.y, fov, z_near, z_far),
    }
}
