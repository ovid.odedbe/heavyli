pub fn get_view_matrix(position: &glm::Vec3, pitch: f32, yaw: f32) -> glm::Mat4 {
    let normalized_front = get_normalized_front(pitch, yaw);

    let center = *position + normalized_front;

    let up = get_up(normalized_front);

    // Create the complete view matrix with look-at:
    return glm::look_at(position, &center, &up);
}

///
/// Gets the normalized front vector.
///
/// @ Param pitch: rotation in the x axis.
/// @ Param yaw: rotation in the y axis
pub fn get_normalized_front(pitch: f32, yaw: f32) -> glm::Vec3 {
    let front = glm::vec3(
        yaw.to_radians().cos() * pitch.to_radians().cos(),
        pitch.to_radians().sin(),
        yaw.to_radians().sin() * pitch.to_radians().cos(),
    );

    return front.normalize();
}

pub fn get_right(normalized_front: glm::Vec3) -> glm::Vec3 {
    let up_position = glm::vec3(0.0, 1.0, 0.0);
    return normalized_front.cross(&up_position).normalize();
}

pub fn get_up(normalized_front: glm::Vec3) -> glm::Vec3 {
    return get_right(normalized_front)
        .cross(&normalized_front)
        .normalize();
}
