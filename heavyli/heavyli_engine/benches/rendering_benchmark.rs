// Crates:
extern crate nalgebra_glm as glm;

// Modules:
use criterion::{criterion_group, criterion_main, Criterion};

use heavyli::{opengl_modules::init_glfw, rendering::window::Window};

use heavyli_engine::{
    ecs::scene::{Scene, SceneCore, SceneState, Update},
    render::{
        camera::Camera,
        utils::{configure_window, window_end_frame, window_start_frame},
    },
};

// Screen Size:
const SCR_WIDTH: u32 = 800;
const SCR_HEIGHT: u32 = 600;

struct Scene1;

impl Scene1 {
    fn new() -> Self {
        Self {}
    }
}

impl Update for Scene1 {
    fn start(&mut self, core: &mut SceneCore) {
        // Add camera to the scene:
        core.registry.lock().unwrap().add_component(
            0,
            Camera::new(glm::vec3(0.0, 0.0, -5.0), glm::vec2(0.0, 90.0)),
        );

        // Create a new script handler:
        let script_id = core.lua_script_manager.create_script_handler();

        // Load the test script:
        if let Err(err) = core
            .lua_script_manager
            .script_load(script_id, "res/bench.lua")
        {
            println!("Error: {}", err);
        }
    }

    fn update(&mut self, core: &mut SceneCore) {
        window_start_frame(core.window);

        // Get camera view for world-location calculations:
        let cam_view = core
            .registry
            .lock()
            .unwrap()
            .get_component::<Camera>(0)
            .unwrap()
            .borrow_mut()
            .lock()
            .unwrap()
            .get_view();

        // Render all sprites:
        core.renderer
            .render(glm::vec2(SCR_WIDTH as f32, SCR_HEIGHT as f32), &cam_view);

        // End scene when window is closed:
        if !core.window.is_open() {
            core.state = SceneState::End;
        }

        // IMPORTANT: remove all sprites' data at the end of the program:
        if SceneState::End == core.state {
            core.renderer.delete_all_sprites();
        }

        window_end_frame(core.window, core.glfw);
    }
}

fn rendering_benchmark(c: &mut Criterion) {
    // Initialize OpenGL with GLFW and create a new Window:
    let mut glfw = init_glfw();
    let mut window = Window::new(&glfw, "Sandbox", SCR_WIDTH, SCR_HEIGHT);

    // Configure the new window:
    configure_window(&mut window);

    // Create a new scene:
    let mut scene = Scene::new(&mut glfw, &mut window, Scene1::new());

    // Initial scene state:
    scene.start();

    // Benchmark the update function:
    c.bench_function("Rendering Test", |b| {
        b.iter(|| {
            scene.update(120.0);
        });
    });
}

criterion_group!(benches, rendering_benchmark);
criterion_main!(benches);
