function start()
    math.randomseed(os.time())

    -- Load new textures and save their IDs:
    mario_texture = add_texture("res/mario-stand.png")
    block_texture = add_texture("res/basic-block.png")

    -- Add new sprites:
    renderer:add_sprite(0, 0.0, 0.0, 0.5, 0.5, mario_texture)
    renderer:add_sprite(2, 1.0, 1.0, 0.5, 0.5, block_texture)
    renderer:add_sprite(3, 0.5, 1.0, 0.5, 0.5, block_texture)
    renderer:add_sprite(4, 1.0, 0.5, 0.5, 0.5, block_texture)
    renderer:add_sprite(5, 0.5, 0.5, 0.5, 0.5, block_texture)
end

counter = 6
mario_texture = 0
block_texture = 0

function update()
    renderer:add_sprite(counter, counter % 12 * 0.5, math.random() % 30, 0.5, 0.5, block_texture)

    counter = counter + 1
end
