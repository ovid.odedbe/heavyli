#version 330 core

out vec4 FragColor;

in vec3 ourColor;
in vec2 texCoord;

uniform sampler2D texture1;

void main()
{
    vec4 col = texture(texture1, texCoord) * vec4(ourColor, 1.0f);

    if (0 == col.r && 0 == col.g && 0 == col.b)
    {
        discard;
    }
    
    FragColor = col;
}
