#version 330 core

layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aColor;
layout (location = 2) in vec3 aNormal;
layout (location = 3) in vec2 aTexCoord;

out vec3 ourColor;
out vec2 texCoord;
out vec3 normal;
out vec3 fragPos;

uniform mat4 translation;
uniform mat4 model;
uniform vec3 lightPos;
uniform vec3 lightColor;

void main()
{
    gl_Position = translation * vec4(aPos, 1.0);
    fragPos = vec3(model * vec4(aPos, 1.0));
    ourColor = aColor;
    texCoord = aTexCoord;
    normal = aNormal;
}
