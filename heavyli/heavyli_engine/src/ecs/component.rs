use downcast::{impl_downcast, DowncastSync};
use heavyli::{
    rendering::shape::{buffers::Buffers, vertices::Vertex},
    transform::transform::Transform,
};

pub trait Component: DowncastSync {}

impl_downcast!(sync Component);

// For 2D rendering:
impl Component for u32 {}
impl Component for glm::Vec3 {}
impl Component for Buffers {}
impl Component for Transform {}
impl Component for Vec<Vertex> {}
