use crate::ecs::component::Component;
use std::collections::hash_map::IterMut;
use std::collections::HashMap;

pub struct ComponentMap {
    components: HashMap<usize, Box<dyn Component>>,
}

impl ComponentMap {
    pub fn new() -> Self {
        Self {
            components: HashMap::<usize, Box<dyn Component>>::new(),
        }
    }

    pub fn add_component<T>(&mut self, entity_id: usize, component: T)
    where
        T: Component,
        T: 'static,
    {
        self.components.insert(entity_id, Box::new(component));
    }

    pub fn get_component<T>(&mut self, entity_id: usize) -> Option<&mut T>
    where
        T: Component,
        T: 'static,
    {
        return match self.components.get_mut(&entity_id) {
            Some(component) => component.downcast_mut::<T>(),
            None => None,
        };
    }

    pub fn get_view(&mut self) -> IterMut<usize, Box<dyn Component>> {
        return self.components.iter_mut();
    }

    pub fn size(&self) -> usize {
        return self.components.len();
    }
}
