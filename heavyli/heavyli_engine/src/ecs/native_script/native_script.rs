use crate::ecs::registry::Registry;
use std::sync::{Arc, Mutex};

pub trait NativeScript {
    // Runs at the first frame of the program.
    fn start(&mut self, registry: Arc<Mutex<Registry>>);

    // Runs at each iteration of the game loop.
    fn update(&mut self, delta_time: &f32, registry: Arc<Mutex<Registry>>);

    // Runs at the last frame of the program.
    fn end(&mut self, registry: Arc<Mutex<Registry>>);
}
