use crate::ecs::{native_script::native_script::NativeScript, registry::Registry};
use std::sync::{Arc, Mutex};

pub struct NativeScriptManager {
    registry: Arc<Mutex<Registry>>,
    native_scripts: Vec<Box<dyn NativeScript>>,
}

impl NativeScriptManager {
    pub fn new(registry: Arc<Mutex<Registry>>) -> Self {
        Self {
            registry: registry,
            native_scripts: vec![],
        }
    }

    pub fn add_script(&mut self, script: Box<dyn NativeScript>) {
        self.native_scripts.push(script);
    }

    pub fn start_scripts(&mut self) {
        self.native_scripts
            .iter_mut()
            .for_each(|native_script| native_script.start(self.registry.clone()));
    }

    pub fn update_scripts(&mut self, delta_time: &f32) {
        self.native_scripts
            .iter_mut()
            .for_each(|native_script| native_script.update(delta_time, self.registry.clone()));
    }

    pub fn end_scripts(&mut self) {
        self.native_scripts
            .iter_mut()
            .for_each(|native_script| native_script.end(self.registry.clone()));
    }
}
