use crate::ecs::{component::Component, component_map::ComponentMap};
use std::any::type_name;
use std::cell::RefCell;
use std::collections::HashMap;
use std::sync::Mutex;

pub struct Registry {
    components: HashMap<String, ComponentMap>,
}

impl Registry {
    pub fn new() -> Self {
        Self {
            components: HashMap::<String, ComponentMap>::new(),
        }
    }

    pub fn add_component<T>(&mut self, entity_id: usize, component: T)
    where
        T: Component,
        T: 'static,
    {
        self.components
            .entry(type_name::<T>().to_string())
            .or_insert(ComponentMap::new())
            .add_component(entity_id, component);
    }

    pub fn get_component<T>(&mut self, entity_id: usize) -> Option<RefCell<Mutex<&mut T>>>
    where
        T: Component,
        T: 'static,
    {
        return match self.components.get_mut(type_name::<T>()) {
            Some(component_map) => match component_map.get_component::<T>(entity_id) {
                Some(component) => Some(RefCell::new(Mutex::new(component))),
                None => None,
            },
            None => None,
        };
    }

    // Gets the view of all entities with the component type T.
    pub fn get_view<T>(&mut self) -> Option<Vec<usize>>
    where
        T: Component,
        T: 'static,
    {
        return match self.components.get_mut(type_name::<T>()) {
            Some(component_map) => Some(
                component_map
                    .get_view()
                    .map(|(entity_id, _component)| *entity_id)
                    .collect(),
            ),
            None => None,
        };
    }

    pub fn size<T>(&self) -> usize
    where
        T: Component,
        T: 'static,
    {
        return self.components[type_name::<T>()].size();
    }
}
