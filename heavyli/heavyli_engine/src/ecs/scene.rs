use crate::ecs::{native_script::native_script_manager::NativeScriptManager, registry::Registry};
use crate::lua_script::lua_script_manager::LuaScriptManager;

use crate::heavyli::rendering::{shader, shape::render};

use crate::render::renderer_2d::{
    generate_sprite_2d_buffers, generate_sprite_2d_vertices, Renderer2D,
};

use std::sync::{Arc, Mutex};
use std::time::Instant;

#[repr(C)]
#[derive(Copy, Clone, PartialEq)]
pub enum SceneState {
    Start,
    Update,
    End,
}

pub struct SceneCore<Globals> {
    pub state: SceneState,
    pub registry: Arc<Mutex<Registry>>,
    pub globals: Arc<Mutex<Globals>>,
    pub native_script_manager: NativeScriptManager,
    pub lua_script_manager: LuaScriptManager,
    pub delta_time: f32,
}

impl<Globals> SceneCore<Globals> {
    pub fn new(globals: Arc<Mutex<Globals>>) -> Self {
        let registry = Arc::new(Mutex::new(Registry::new()));
        let mut vertices = generate_sprite_2d_vertices();
        let shader_id = shader::compile("shaders/basic_vertex.glsl", "shaders/basic_fragment.glsl");
        let renderer = Renderer2D::new(
            registry.clone(),
            generate_sprite_2d_buffers(&mut vertices),
            vertices,
            shader_id,
        );

        Self {
            state: SceneState::Start,
            native_script_manager: NativeScriptManager::new(registry.clone()),
            lua_script_manager: LuaScriptManager::new(renderer.clone()),
            delta_time: 0.0,
            registry: registry,
            globals: globals.clone(),
        }
    }
}

pub trait Update<Globals> {
    fn start(&mut self, core: &mut SceneCore<Globals>);
    fn update(&mut self, core: &mut SceneCore<Globals>);
}

pub struct Scene<Globals> {
    repr: Box<dyn Update<Globals>>,
    core: SceneCore<Globals>,
}

const NANOS_TO_SECONDS: f32 = 1000000000.0;

impl<Globals> Scene<Globals> {
    pub fn new(scene_loop: Box<dyn Update<Globals>>, globals: Arc<Mutex<Globals>>) -> Self {
        Self {
            repr: scene_loop,
            core: SceneCore::new(globals),
        }
    }

    pub fn get_state(&self) -> SceneState {
        return self.core.state;
    }

    pub fn start(&mut self) {
        // Start Scene:
        self.repr.start(&mut self.core);

        // Start Scripts:
        self.core.lua_script_manager.start_scripts();
        self.core.native_script_manager.start_scripts();

        // Update State:
        self.core.state = SceneState::Update;
    }

    pub fn update(&mut self, preferred_fps: f32) {
        // Time of the beginning of the frame:
        let start_time = Instant::now();

        // Update Scene:
        self.repr.update(&mut self.core);

        // Update Scripts:
        self.core
            .native_script_manager
            .update_scripts(&self.core.delta_time);
        self.core
            .lua_script_manager
            .update_scripts(&self.core.delta_time);

        // Limit the FPS to the preferred value:
        render::limit_fps(start_time, preferred_fps);

        // Update delta time:
        self.core.delta_time = start_time.elapsed().as_nanos() as f32 / NANOS_TO_SECONDS;
    }
}
