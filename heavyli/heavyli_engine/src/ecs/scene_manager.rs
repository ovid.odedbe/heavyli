use crate::ecs::scene::{Scene, SceneState, Update};

use std::sync::{Arc, Mutex};

pub struct SceneManager<Globals> {
    scenes: Vec<Scene<Globals>>,
    globals: Arc<Mutex<Globals>>,
}

impl<Globals> SceneManager<Globals> {
    pub fn new(globals: Arc<Mutex<Globals>>) -> Self {
        Self {
            scenes: Vec::new(),
            globals: globals,
        }
    }

    pub fn new_scene(&mut self, scene_loop: Box<dyn Update<Globals>>) -> usize {
        self.scenes
            .push(Scene::new(scene_loop, self.globals.clone()));

        return self.scenes.len() - 1;
    }

    pub fn start_scene(&mut self, scene_id: usize) {
        if let Some(scene) = self.scenes.get_mut(scene_id) {
            scene.start();
        } else {
            println!("Invalid Scene ID");
        }
    }

    pub fn update_scene(&mut self, scene_id: usize, preferred_fps: f32) {
        if let Some(scene) = self.scenes.get_mut(scene_id) {
            scene.update(preferred_fps);
        } else {
            println!("Invalid Scene ID");
        }
    }

    pub fn get_scene_state(&self, scene_id: usize) -> Result<SceneState, &'static str> {
        if let Some(scene) = self.scenes.get(scene_id) {
            return Ok(scene.get_state());
        } else {
            return Err("Invalid Scene ID");
        }
    }
}
