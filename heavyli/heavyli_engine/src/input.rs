use device_query::{DeviceQuery, DeviceState, Keycode};
use lazy_static::lazy_static;
use std::sync::Mutex;

lazy_static! {
    pub static ref PRESSED_KEYS: Mutex<Vec<Keycode>> = Mutex::new(Vec::new());
}

pub fn handle_glfw_window_event(event: glfw::WindowEvent) {
    match event {
        glfw::WindowEvent::FramebufferSize(width, height) => unsafe {
            gl::Viewport(0, 0, width, height)
        },
        _ => {}
    }
    let device_state = DeviceState::new();

    PRESSED_KEYS.lock().unwrap().clear();
    PRESSED_KEYS
        .lock()
        .unwrap()
        .append(&mut device_state.get_keys());
}

pub fn key_pressed(key_name: String) -> bool {
    // Match Keys to Keycodes:
    let keycode = match key_name.as_str() {
        "w" => Some(Keycode::W),
        "a" => Some(Keycode::A),
        "s" => Some(Keycode::S),
        "d" => Some(Keycode::D),
        "z" => Some(Keycode::Z),
        "x" => Some(Keycode::X),
        "c" => Some(Keycode::C),
        "up" => Some(Keycode::Up),
        "down" => Some(Keycode::Down),
        "left" => Some(Keycode::Left),
        "right" => Some(Keycode::Right),
        _ => None,
    };

    if let Some(keycode) = keycode {
        // Check if the key is pressed
        return PRESSED_KEYS.lock().unwrap().contains(&keycode);
    } else {
        // If the key doesn't exist, return it's not pressed:
        return false;
    }
}
