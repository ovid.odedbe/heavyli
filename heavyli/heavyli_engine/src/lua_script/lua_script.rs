use crate::file_utils::read_lines;
use crate::input;
use crate::render::renderer_2d::Renderer2D;
use rlua::{Function, Lua};

pub struct LuaScript {
    lua: Lua,
}

impl LuaScript {
    pub fn new(renderer: Renderer2D) -> Self {
        let lua = Lua::new();

        lua.context(|lua_ctx| {
            lua_ctx.globals().set("renderer", renderer).unwrap();
        });

        Self { lua: lua }
    }

    pub fn load(&mut self, script_path: &str) -> Result<(), String> {
        // Read all lines seperately:
        let lines = read_lines(script_path);

        // Save script content in this string:
        let mut script = String::new();

        if let Ok(lines) = lines {
            // Execute each line of the lua file:
            for line in lines {
                match line {
                    Ok(mut line) => {
                        // Add newline for the interperter to read:
                        line.push('\n');

                        // Add line to full script:
                        script.push_str(line.as_str());
                    }
                    Err(err) => {
                        return Err(format!(
                            "Failed to read file '{}', details: {}",
                            script_path, err
                        ));
                    }
                }
            }
        } else {
            return Err(format!("Failed to load file '{}'", script_path));
        }

        self.lua.context(|lua_ctx| {
            let key_pressed = lua_ctx
                .create_function(|_, (key_name,): (String,)| {
                    return Ok(input::key_pressed(key_name));
                })
                .unwrap();

            let add_texture = lua_ctx
                .create_function(|_, (image,): (String,)| {
                    return Ok(Renderer2D::add_texture(image.as_str()));
                })
                .unwrap();

            lua_ctx.globals().set("key_pressed", key_pressed).unwrap();
            lua_ctx.globals().set("add_texture", add_texture).unwrap();

            // Load script to lua handler:
            match lua_ctx.load(script.as_str()).exec() {
                Ok(()) => println!("Script executed successfully"),
                Err(err) => {
                    println!("Error executing script, details: {}", err);
                    return;
                }
            }
        });

        return Ok(());
    }

    pub fn set_delta_time(&mut self, delta_time: &f32) {
        self.lua.context(|lua_ctx| {
            // Set delta_time global variable:
            let result = lua_ctx.globals().set("delta_time", *delta_time);

            // Check for errors:
            if let Err(err) = result {
                println!("Failed to set delta time, details: {}", err);
            }
        });
    }

    pub fn call_function(&self, function_name: &str) {
        self.lua.context(|lua_ctx| {
            // Run inputted function from script:
            match lua_ctx.globals().get::<_, Function>(function_name) {
                // Load from globals.
                Ok(start_function) => {
                    // Try execute function:
                    if let Err(err) = start_function.call::<_, ()>(()) {
                        println!("Error in function '{}', details: {}", function_name, err);
                    }
                }
                Err(err) => println!("Error with '{}' function, details: {}", function_name, err),
            }
        });
    }

    // TODO: Find better solution instead of making a script render.
    pub fn render(&self, screen_size: glm::Vec2, cam_view: &glm::Mat4) {
        self.lua.context(|lua_ctx| {
            let renderer = &mut lua_ctx.globals().get::<_, Renderer2D>("renderer").unwrap();

            renderer.render(screen_size, cam_view);
        });
    }
}
