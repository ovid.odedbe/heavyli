use crate::lua_script::lua_script::LuaScript;
use crate::render::renderer_2d::Renderer2D;
use queues::*;
use std::collections::HashMap;

pub struct LuaScriptManager {
    lua_scripts: HashMap<usize, LuaScript>,
    last_id: usize,
    ids: Queue<usize>,
    renderer: Renderer2D,
}

impl LuaScriptManager {
    pub fn new(renderer: Renderer2D) -> Self {
        Self {
            lua_scripts: HashMap::<usize, LuaScript>::new(),
            last_id: 1,
            ids: queue![0],
            renderer: renderer,
        }
    }

    // Creates a new script handler and returns its ID.
    pub fn create_script_handler(&mut self) -> usize {
        let script_id;

        // Get an avialable ID from the ID queue:
        match self.ids.remove() {
            Ok(id) => script_id = id,
            Err(_) => {
                // If failed, use new ID from last ID.
                script_id = self.last_id;
                self.last_id += 1; // Next ID.
            }
        }

        // Add new Lua Script to map:
        self.lua_scripts
            .insert(script_id, LuaScript::new(self.renderer.clone()));

        return script_id;
    }

    // Loads a script from a lua file to the chosen handler.
    pub fn script_load(&mut self, script_id: usize, script_path: &str) -> Result<(), String> {
        if let Some(script) = self.lua_scripts.get_mut(&script_id) {
            return script.load(script_path);
        } else {
            return Err("Script ID doesn't exist.".to_string());
        }
    }

    // Sets Delta Time to all scripts in the manager.
    pub fn set_delta_time(&mut self, delta_time: &f32) {
        self.lua_scripts
            .iter_mut()
            .for_each(|(_script_id, script)| {
                script.set_delta_time(delta_time);
            });
    }

    pub fn start_scripts(&mut self) {
        self.lua_scripts
            .iter_mut()
            .for_each(|(_script_id, script)| {
                script.call_function("start");
            });
    }

    pub fn update_scripts(&mut self, delta_time: &f32) {
        self.lua_scripts
            .iter_mut()
            .for_each(|(_script_id, script)| {
                script.set_delta_time(delta_time);
                script.call_function("update");
            });
    }
}
