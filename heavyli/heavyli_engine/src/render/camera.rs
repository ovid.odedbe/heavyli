use crate::ecs::component::Component;
use crate::heavyli::transform::view;

#[derive(Copy, Clone)]
pub struct Camera {
    position: glm::Vec3,
    rotation: glm::Vec2,
}

impl Component for Camera {}

impl Camera {
    pub fn new(position: glm::Vec3, rotation: glm::Vec2) -> Self {
        Self {
            position: position,
            rotation: rotation,
        }
    }

    pub fn set_position(&mut self, position: glm::Vec3) {
        self.position = position;
    }

    pub fn get_position_z(&self) -> f32 {
        return self.position.z;
    }

    pub fn get_view(&self) -> glm::Mat4 {
        return view::get_view_matrix(&self.position, self.rotation.x, self.rotation.y);
    }
}
