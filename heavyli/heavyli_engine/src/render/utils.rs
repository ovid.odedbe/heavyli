use crate::heavyli::rendering::{
    shape::{
        buffers::{self, Buffers},
        vertices::{DrawType, Vertex},
    },
    texture_2d,
    window::{ClearColor, Window},
};

use crate::input;

pub unsafe fn init_texture(texture_id: u32, texture_image_directory: &str, is_alpha: bool) {
    texture_2d::bind(texture_id);
    texture_2d::load_image(texture_image_directory.to_string(), is_alpha);
    texture_2d::unbind();
}

pub unsafe fn init_buffers(buffers: &Buffers, vertices: &mut Vec<Vertex>) {
    buffers::bind_buffers(buffers);

    texture_2d::attach(vertices); // Attach the texture coordinates to fit the given vertices.

    buffers::bind_data(buffers.vao, vertices);

    buffers::update_vertex_attributes(DrawType::Triangles); // Let OpenGL know that the shape is based on Triangles.

    buffers::unbind();
}

pub fn configure_window(window: &mut Window) {
    window.make_current();
    window.set_key_polling(true);
    window.set_framebuffer_size_polling(true);

    window.load_function_pointers();
}

pub fn window_start_frame(window: &mut Window) {
    // Process Window Events:
    window.process_events();

    // Refresh Window:
    unsafe {
        Window::clear(ClearColor {
            red: 0.3,
            green: 0.5,
            blue: 1.0,
            alpha: 1.0,
        });
    }
}

pub fn window_end_frame(window: &mut Window) {
    window.swap_buffers();
    for (_, event) in glfw::flush_messages(&window.events()) {
        input::handle_glfw_window_event(event);
    }
}
