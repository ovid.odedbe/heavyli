function start()
    math.randomseed(os.time())

    -- Load new textures and save their IDs:
    mario_texture = add_texture("res/mario-stand.png")
    block_texture = add_texture("res/basic-block.png")

    -- Add new sprites:
    renderer:add_sprite(0, 0.0, 0.0, 0.5, 0.5, mario_texture)
    renderer:add_sprite(2, 1.0, 1.0, 0.5, 0.5, block_texture)
    renderer:add_sprite(3, 0.5, 1.0, 0.5, 0.5, block_texture)
    renderer:add_sprite(4, 1.0, 0.5, 0.5, 0.5, block_texture)
    renderer:add_sprite(5, 0.5, 0.5, 0.5, 0.5, block_texture)
end

counter = 6
pos_x = 0
pos_y = 0
speed = 1
mario_texture = 0
block_texture = 0

function update()
    speed = delta_time

    -- User input:
    if key_pressed("up") then
        pos_y = pos_y + speed
    elseif key_pressed("down") then
        pos_y = pos_y - speed
    end

    if key_pressed("left") then
        pos_x = pos_x + speed
    elseif key_pressed("right") then
        pos_x = pos_x - speed
    end

    renderer:set_sprite_position(0, pos_x, pos_y)
    renderer:set_camera_position(0, pos_x, pos_y)
    
    -- Randbom block generation:
    if key_pressed("a") then
        renderer:add_sprite(counter, counter % 12 * 0.5, math.random() % 30, 0.5, 0.5, block_texture)

        counter = counter + 1
        print(counter)
    end
end
