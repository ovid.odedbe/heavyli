#version 330 core

out vec4 FragColor;

in vec3 ourColor;
in vec2 texCoord;
in vec3 normal;
in vec3 fragPos;

uniform float ambientStrength;
uniform vec3 lightPos;
uniform vec3 lightColor;

void main()
{
    vec3 norm = normalize(normal);
    vec3 lightDir = normalize(lightPos - fragPos);

    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = diff * lightColor;

    vec3 ambient = ourColor * ambientStrength;

    vec3 result = (ambient + diffuse);

    FragColor = vec4(result, 1.0f);
}
